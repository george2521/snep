<?php

/**
 * Class to manager Pauses in mongodb
 *
 * @see Q-Manager
 *
 * @category  Snep
 * @package   Q-Manager
 * @copyright Copyright (c) 2016 Opens Tecnologia
 * @author    Tiago Zimmermann <tiago.zimmermann@opens.com.br>
 *
 */
class Pauses_Manager {

    /**
     * Method to get all pauses
     * @return <array>
     */
    public static function getAll($url) {

        $http = curl_init($url);
        curl_setopt($http, CURLOPT_SSL_VERIFYPEER, false);
        $status = curl_getinfo($http, CURLINFO_HTTP_CODE);
        curl_setopt($http, CURLOPT_RETURNTRANSFER,1);
        $http_response = curl_exec($http);
        $httpcode = curl_getinfo($http, CURLINFO_HTTP_CODE);
        curl_close($http);

        switch ($httpcode) {
            case 200:
                $data = json_decode($http_response);
                break;
            default:
                $data = $httpcode;
                break;
        }

            return $data;
    }

    /**
     * Method to get pause by code
     * @param <int> $id
     * @return <array>
     */
    public static function getPauseByCode($url) {

        $http = curl_init($url);
        curl_setopt($http, CURLOPT_SSL_VERIFYPEER, false);
        $status = curl_getinfo($http, CURLINFO_HTTP_CODE);
        curl_setopt($http, CURLOPT_RETURNTRANSFER,1);
        $http_response = curl_exec($http);
        $httpcode = curl_getinfo($http, CURLINFO_HTTP_CODE);
        curl_close($http);

        switch ($httpcode) {
            case 200:
                $data = json_decode($http_response);
                break;
            default:
                $data = $httpcode;
                break;
        }

            return $data;
    }

    /*
    * Add pause
    * @param <array> $pause
    */
    public static function add($pause, $url) {

        $data = array("code" => $pause["code"], "name" => trim($pause["name"]), "limit" => trim($pause["limit"]), "created" => date('Y-m-d H:i:s'), 'updated' => date('Y-m-d H:i:s'));

        $content = json_encode($data);
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json"));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

        $json_response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        return $httpcode;

    }

    /**
     * Method to update a pause data
     * @param <Array> $pause
     */
    public function edit($pause, $url) {

        $data = array("name" => trim($pause["name"]), "limit" => trim($pause["limit"]), "updated" => date('Y-m-d H:i:s'));
        $content['data'] = $data;
        $content['filter']['code'] =  $pause["code"];
        $content = json_encode($content);

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json"));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

        $json_response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        return $httpcode;

    }

    /**
     * Method to remove a pause
     * @param <int> $id
     */
    public function remove($id, $url) {

        $array = array("code" => $id);
        $content = json_encode($array);
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json"));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

        $json_response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        return $httpcode;
    }

}