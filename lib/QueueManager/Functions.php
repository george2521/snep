<?php

/**
 * Class with function for QueueManager
 *
 * @category  Snep
 * @package   Q-Manager
 * @copyright Copyright (c) 2016 Opens Tecnologia
 * @author    Tiago Zimmermann <tiago.zimmermann@opens.com.br>
 *
 */
class QueueManager_Functions {

    /**
     * Method to get only peers/agents with binds
     * @param <int> $userId
     * @param <array> $peers
     * @return <array> $peers
     */
    public static function getPeersBound($userId, $peers) {

        $binds = QueueManager_Manager::getBond($userId);

        if($binds){

            $newPeers = array();
            foreach($binds as $key => $value){
                if($value['type'] == 'bound'){
                    $newPeers[$key]['exten'] = $value['peer_name'];
                    $newPeers[$key]['name'] = $value['callerid'];
                    $bound = true;
                }else{ //nobound
                    foreach($peers as $x => $peer){

                        if($peer['exten'] == $value['peer_name']){
                            unset($peers[$x]);

                        }
                    }
                }
            }

            if(isset($bound)){
                $peers = $newPeers;
            }
        }

        return $peers;

    }


    /**
     * format timestamp em format hh:mm:ss
     * @param <string> $time
     * @return <string> $ret
     */
    public static function fmt_Time($time, $fmt = 'hms') {

        $segundos = $time;

        switch ($fmt) {

            case "m":
                $ret = $segundos / 60;
                break;
            case "H":
                $ret = $segundos / 3600;
                break;
            case "h":
                $ret = round($segundos / 3600);
                break;
            case "D":
                $ret = $segundos / 86400;
                break;
            case "d":
                $ret = round($segundos / 86400);
                break;
            case "hms":
                $min_t = intval($segundos / 60);
                $tsec = sprintf("%02s", intval($segundos % 60));
                $thor = sprintf("%02s", intval($min_t / 60));
                $tmin = sprintf("%02s", intval($min_t % 60));
                $ret = $thor . ":" . $tmin . ":" . $tsec;
                break;
            case "ms":
                $min_t = intval($segundos / 60);
                $tsec = sprintf("%02s", intval($segundos % 60));
                $tmin = sprintf("%02s", intval($min_t % 60));
                $ret = $tmin . ":" . $tsec;
                break;
        }

        return $ret;
    }


    /**
     * FormatDateSelect - receive selectperiod and format date return in format array in format yyyy-mm-dd h:m:d
     * @param <string> $selectPeriod - today,last7days, last15days, last30days, rangedate
     * @return <srray> $dateFormat['start_date'] - $dateFormat['end_date']
     */
    public static function FormatDateSelect($selectPeriod,$from,$to){

        $today = date("Y-m-d");

        if($selectPeriod == 'today'){
            $dateFormat['start_date'] = $today." 00:00:00";
            $dateFormat['end_date'] = $today." 23:59:59";
        }elseif($selectPeriod == 'rangedate'){
            //format date to yyyy-mm-dd
            $format = Snep_Reports::fmt_date($from. " 00:00",$to. " 23:59");
            $dateFormat['start_date'] =  $format['start_date']." 00:00:00";
            $dateFormat['end_date'] =  $format['end_date']." 23:59:59";
        }else{
            $dateFormat['end_date'] = $today." 23:59:59";
            $valueDay = preg_replace("/[^0-9]/", "", $selectPeriod);
            $dateFormat['start_date'] =  date('Y-m-d', strtotime($today. ' - '.$valueDay.' days'))." 00:00:00";
        }

        return $dateFormat;

    }


    /**
     * getPeersWithPermission - get all peers when user have permission
     * @return <array> $peers
     */
    public static function getPeersWithPermission(){

        // Check Bond
        $auth = Zend_Auth::getInstance();
        $username = $auth->getIdentity();
        $user = Snep_Users_Manager::getName($username);

        // get All peers;
        $peers = Snep_Extensions_Manager::getAll();

        // binds
        if($user['id'] != '1'){
            // get only peers with binds
            $peers = self::getPeersBound($user['id'], $peers);
        }

        return $peers;
    }

    /**
     * dacCsvanalytic - format data for export in csv file
     * @param <array> $data
     * @return <string> $values
     */
    public function dacCsvanalytic($data) {

        $i18n = Zend_Registry::get("i18n");

        $values = $i18n->translate('Chamada').','. $i18n->translate('Date').','.$i18n->translate('Fila').','.$i18n->translate('Canal').','.$i18n->translate('Origem').','. $i18n->translate('Tempo').',' .$i18n->translate('Evento')."\n";

        foreach ($data as $queue => $call):
            if($call != false):
            foreach($call as $x => $value):

                $values .= $x.','.$value->header->calldate.','.$value->header->queue.', - ,'. $value->header->from . ','. $value->header->holdtime. ','. $value->header->firstevent. "\n";

                if(isset($value->callflow)):
                    foreach($value->callflow as $y => $callflow):
                        $values .= $x.',';
                        $values .= $callflow->eventdate.',';
                        $values .= (isset($callflow->queue)) ? $callflow->queue.',' : "-,";
                        $values .= (isset($callflow->name)) ? $callflow->name.',' : "-,";
                        $values .= '- ,';
                        $values .= (isset($callflow->holdtime)) ? $callflow->holdtime.',' : "-,";
                        $values .= $i18n->translate($callflow->event). "\n";
                     endforeach;
                endif;
                // footer
                $values .= 'RESUMO,';
                $values .= 'Tempo de Espera: ,'.$value->footer->timeWaiting.',';
                $values .= 'Tempo de Atendimento: ,'.$value->footer->talktime.',';
                $values .= 'Tempo de Chamada: ,'.$value->footer->duration.',';
                $values .= "\n\n";
            endforeach;
            $values .= "\n";


            endif;
        endforeach;

        return $values;
    }

    /**
     * dacCsvanalytic - format data for export in csv file
     * @param <array> $data
     * @return <string> $values
     */
    public function dacCsvsyntetic($data) {

        $i18n = Zend_Registry::get("i18n");
        $valuesHeader = $i18n->translate('Fila').','.$i18n->translate('Total').','.$i18n->translate('Atendida').','.$i18n->translate('Abandonadas').','.$i18n->translate('Tempo Limite').','. $i18n->translate('Transferidas').','.$i18n->translate('Desligadas pelo Operador').','.$i18n->translate('Desligadas pelo Agente').','. $i18n->translate('TME').',' .$i18n->translate('TMA').',' .$i18n->translate('TMC')."\n";

        foreach($data as $queue => $data):
            $divTotal = (isset($data->total) ? $data->total : 1 );
            $divAnswered = (isset($data->answered) ? $data->answered : 1 );
            $valuesHeader .= $queue.",";
            $valuesHeader .= $data->total.",";
            $valuesHeader .= $data->answered. "(".round(( $data->answered * 100 ) / $divTotal)."%) ,";
            $valuesHeader .= $data->abandon. "(".round(( $data->abandon * 100 ) / $divTotal)."%) ,";
            $valuesHeader .= $data->timeout. "(".round(( $data->timeout * 100 ) / $divTotal)."%) ,";
            $valuesHeader .= $data->transfer. "(".round(( $data->transfer * 100 ) / $divAnswered)."%) ,";
            $valuesHeader .= $data->agent. "(".round(( $data->agent * 100 ) / $divAnswered)."%) ,";
            $valuesHeader .= $data->caller. "(".round(( $data->caller * 100 ) / $divAnswered)."%) ,";
            $valuesHeader .= QueueManager_Functions::fmt_Time($data->holdtime / $divTotal).",";
            $valuesHeader .= QueueManager_Functions::fmt_Time($data->talktime / $divAnswered).",";
            $valuesHeader .= QueueManager_Functions::fmt_Time($data->duration / $divTotal)."\n";

        endforeach;

        return $valuesHeader;

    }

    /**
     * OoperatorsCsv- format data for export in csv file
     * @param <array> $data
     * @return <string> $values
     */
    public function operatorsCsv($data) {

        $i18n = Zend_Registry::get("i18n");

        $valuesHeaderTotal = $i18n->translate('Operador').','.$i18n->translate('Tipo').','.$i18n->translate('Total').','.$i18n->translate('Atendidas').','.$i18n->translate('Não Atendidas').','. $i18n->translate('TME').',' .$i18n->translate('TMA').',' .$i18n->translate('TMC')."\n";

        $valuesHeader = $i18n->translate('Operador').','.$i18n->translate('Data').','.$i18n->translate('Tipo').','.$i18n->translate('Total').','.$i18n->translate('Atendidas').','.$i18n->translate('Não Atendidas').','. $i18n->translate('TME').',' .$i18n->translate('TMA').',' .$i18n->translate('TMC')."\n";

        foreach($data as $operator => $parc):

            foreach($parc->total as $type => $valu):

                if($type == "incoming"):
                    $divTotalIn =   ( isset($parc->total->incoming->total)  ? $parc->total->incoming->total  : 1 );
                    $divTAnswerIn = ( isset($parc->total->incoming->answer) ? $parc->total->incoming->answer : 1 );

                    $valuesHeaderTotal .= $operator.",";
                    $valuesHeaderTotal .= $i18n->translate('Recebidas').",";
                    $valuesHeaderTotal .= $parc->total->incoming->total.",";
                    $valuesHeaderTotal .= $parc->total->incoming->answer.",";
                    $valuesHeaderTotal .= $parc->total->incoming->noanswer.",";
                    $valuesHeaderTotal .= QueueManager_Functions::fmt_Time($parc->total->incoming->ringtime / $divTotalIn).",";
                    $valuesHeaderTotal .= QueueManager_Functions::fmt_Time($parc->total->incoming->talktime / $divTAnswerIn).",";
                    $valuesHeaderTotal .= QueueManager_Functions::fmt_Time($parc->total->incoming->duration / $divTotalIn)."\n";
                endif;

                if($type == "outgoing"):

                    $divTotalOut =   ( isset($parc->total->outgoing->total)  ? $parc->total->outgoing->total  : 1 );
                    $divTAnswerOut = ( isset($parc->total->outgoing->answer) ? $parc->total->outgoing->answer : 1 );

                    $valuesHeaderTotal .= $operator.",";
                    $valuesHeaderTotal .= $i18n->translate('Originadas').",";
                    $valuesHeaderTotal .= $parc->total->outgoing->total.",";
                    $valuesHeaderTotal .= $parc->total->outgoing->answer.",";
                    $valuesHeaderTotal .= $parc->total->outgoing->noanswer.",";
                    $valuesHeaderTotal .= QueueManager_Functions::fmt_Time($parc->total->outgoing->ringtime / $divTotalOut).",";
                    $valuesHeaderTotal .= QueueManager_Functions::fmt_Time($parc->total->outgoing->talktime / $divTAnswerOut).",";
                    $valuesHeaderTotal .= QueueManager_Functions::fmt_Time($parc->total->outgoing->duration / $divTotalOut)."\n";

                endif;

            endforeach;

            foreach($parc->parciais as $date => $val):

                if(isset($val->outgoing)):
                    $divParcTotalOut = ( isset($val->outgoing->total) ? $val->outgoing->total : 1 );
                    $divParcAnswerOut = ( isset($val->outgoing->answer) ? $val->outgoing->answer : 1 );

                    $valuesHeader .= $operator.",";
                    $valuesHeader .= $date.",";
                    $valuesHeader .= $i18n->translate('Originadas').",";
                    $valuesHeader .= $val->outgoing->total.",";
                    $valuesHeader .= (isset($val->outgoing->answer)) ? $val->outgoing->answer ."(".round(( $val->outgoing->answer * 100 ) / $divParcTotalOut)."%),": '0,';
                    $valuesHeader .= (isset($val->outgoing->noanswer)) ? $val->outgoing->noanswer ."(".round(( $val->outgoing->noanswer * 100 ) / $divParcTotalOut)."%),": '0,';
                    $valuesHeader .= QueueManager_Functions::fmt_Time($val->outgoing->ringtime / $divParcTotalOut).",";
                    $valuesHeader .= QueueManager_Functions::fmt_Time($val->outgoing->talktime / $divParcAnswerOut).",";
                    $valuesHeader .= QueueManager_Functions::fmt_Time($val->outgoing->duration / $divParcTotalOut)."\n";
                endif;

                if(isset($val->incoming)):
                    $divParcTotalIn = ( isset($val->incoming->total) ? $val->incoming->total : 1 );
                    $divParcAnswerIn = ( isset($val->incoming->answer) ? $val->incoming->answer : 1 );

                    $valuesHeader .= $operator.",";
                    $valuesHeader .= $date.",";
                    $valuesHeader .= $i18n->translate('Recebidas').",";
                    $valuesHeader .= $val->incoming->total.",";
                    $valuesHeader .= (isset($val->incoming->answer)) ? $val->incoming->answer ."(".round(( $val->incoming->answer * 100 ) / $divParcTotalIn)."%),": '0,' ;
                    $valuesHeader .= (isset($val->incoming->noanswer)) ? $val->incoming->noanswer ."(".round(( $val->incoming->noanswer * 100 ) / $divParcTotalIn)."%),": '0,' ;
                    $valuesHeader .= QueueManager_Functions::fmt_Time($val->incoming->ringtime / $divParcTotalIn).",";
                    $valuesHeader .= QueueManager_Functions::fmt_Time($val->incoming->talktime / $divParcAnswerIn).",";
                    $valuesHeader .= QueueManager_Functions::fmt_Time($val->incoming->duration / $divParcTotalIn)."\n";
                endif;
            endforeach;
        endforeach;

        $result = $valuesHeaderTotal."\n".$valuesHeader;

        return $result;

    }

}
