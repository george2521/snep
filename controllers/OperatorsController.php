<?php

/**
 * Q-Manager Agents Controller
 *
 * @category  Snep
 * @package   Q-Manager
 * @copyright Copyright (c) 2016 OpenS Tecnologia
 * @author    Tiago Zimmermann <tiago.zimmermann@opens.com.br>
 */
class SnepQManager_OperatorsController extends Zend_Controller_Action {

    /**
     * Initial settings of the class
     */
     public function init() {
        $this->view->url = $this->getFrontController()->getBaseUrl() . '/' . $this->getRequest()->getModuleName(). '/' . $this->getRequest()->getControllerName();
        $this->view->lineNumber = Zend_Registry::get('config')->ambiente->linelimit;

        $this->connector = "http://127.0.0.1:3000/config/agents";

        $this->view->baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl();
        $this->view->key = Snep_Dashboard_Manager::getKey(
            Zend_Controller_Front::getInstance()->getRequest()->getModuleName(),
            Zend_Controller_Front::getInstance()->getRequest()->getControllerName(),
            Zend_Controller_Front::getInstance()->getRequest()->getActionName());
    }

    /**
     * List all Operators
     */
    public function indexAction() {

        $this->view->breadcrumb = Snep_Breadcrumb::renderPath(array(
                    $this->view->translate("Q-Manager"),
                    $this->view->translate("Operators")));

        $agents = Operators_Manager::getAll($this->connector);

        if(is_int($agents) && $agents != 204){
            $message = $this->view->translate("Error: Code ") . $agents . $this->view->translate(". Please contact the administrator.");
            $this->_helper->redirector('sneperror','error','default',array('error_message'=>$message));
        }else{
            $this->view->agents = $agents;
        }

    }

    /**
     * Add operator
     */
    public function addAction(){

        $this->view->breadcrumb = Snep_Breadcrumb::renderPath(array(
                    $this->view->translate("Q-Manager"),
                    $this->view->translate("Operators"),
                    $this->view->translate("Add")));

        $this->view->queues = QueueManager_Manager::getQueueAll();

        $this->renderScript( $this->getRequest()->getControllerName().'/add.phtml' );

        if($this->_request->getPost()) {

            $dados = $this->_request->getParams();
            $url = $this->connector."/?agent=".$dados['code'];
            $agent = Operators_Manager::getAgentByCode($url);

            // code no exists
            if($agent == 204){

                $httpcode = Operators_Manager::add($dados, $this->connector);

                if($httpcode = 200){
                    $this->_redirect($this->getRequest()->getModuleName().'/'.$this->getRequest()->getControllerName());
                }else{
                    $message = $this->view->translate("Error: Code ") . $httpcode . $this->view->translate(". Please contact the administrator.");
                    $this->_helper->redirector('sneperror','error','default',array('error_message'=>$message));
                }

            }elseif($agent[0]){
                $message = $this->view->translate("Code already exists.");
                $this->_helper->redirector('sneperror','error','default',array('error_message'=>$message));
            }else{
                $message = $this->view->translate("Error: Code  ") . $agent['statuscode'] . $this->view->translate(". Please contact the administrator.");
                $this->_helper->redirector('sneperror','error','default',array('error_message'=>$message));
            }
        }

    }

    /**
     * Add multiples operators
     */
    public function addmultAction(){

        $this->view->breadcrumb = Snep_Breadcrumb::renderPath(array(
                    $this->view->translate("Q-Manager"),
                    $this->view->translate("Operators"),
                    $this->view->translate("Adicionar Múltiplos")));

        $this->view->queues = QueueManager_Manager::getQueueAll();

        $this->renderScript( $this->getRequest()->getControllerName().'/addmult.phtml' );

        if($this->_request->getPost()) {

            $dados = $this->_request->getParams();
            $codeInit = $dados["codeInit"];
            $codeEnd = $dados["codeEnd"];

            if($codeEnd > $codeInit){

                if($codeEnd - $codeInit <= 500){

                    for($code = $codeInit; $code <= $codeEnd; $code++){

                        $url = $this->connector."/?agent=".$code;
                        $agent = Operators_Manager::getAgentByCode($url);

                        // code no exists
                        if($agent == 204){
                            $httpcode = Operators_Manager::addMult((string)$code, $dados, $this->connector);

                            if($httpcode != 200){
                                $message = $this->view->translate("Error: Code ") . $httpcode . $this->view->translate(". Please contact the administrator.");
                                $this->_helper->redirector('sneperror','error','default',array('error_message'=>$message));
                            }
                        }
                    }
                    $this->_redirect($this->getRequest()->getModuleName().'/'.$this->getRequest()->getControllerName());
                }else{
                    $message = $this->view->translate("O limite de operadores é de no máximo 500 por requisição.");
                    $this->_helper->redirector('sneperror','error','default',array('error_message'=>$message));
                }
            }else{
                $message = $this->view->translate("O código inicial deve ser menor que o código final.");
                $this->_helper->redirector('sneperror','error','default',array('error_message'=>$message));
            }

        }

    }


    /**
     * Edit operator
     */
    public function editAction(){

        $this->view->breadcrumb = Snep_Breadcrumb::renderPath(array(
                    $this->view->translate("Q-Manager"),
                    $this->view->translate("Operators"),
                    $this->view->translate("Edit")));

        $id = $this->_request->getParam('id');

        $url = $this->connector."/?agent=".$id;
        $agent = Operators_Manager::getAgentByCode($url);

        $peer_fixed = "";
        if(isset($agent[0]->peer_fixed) && $agent[0]->peer_fixed == true){
            $peer_fixed = 'checked';
        }

        $this->view->peer_fixed = $peer_fixed;

        $this->view->agent = $agent;

        $this->view->queues = QueueManager_Manager::getQueueAll();
        $this->renderScript( $this->getRequest()->getControllerName().'/edit.phtml' );

        // After POST
        if ($this->_request->getPost()) {

            $dados = $this->_request->getParams();
            $httpcode = Operators_Manager::edit($dados, $this->connector);
            $this->_redirect($this->getRequest()->getModuleName().'/'.$this->getRequest()->getControllerName());
        }

    }

    /**
     * Remove operator
     */
    public function removeAction(){

        $this->view->breadcrumb = Snep_Breadcrumb::renderPath(array(
                    $this->view->translate("Q-Manager"),
                    $this->view->translate("Operators"),
                    $this->view->translate("Remove")));

        $id = $this->_request->getParam('id');

        $this->view->id = $id;
        $this->view->remove_title = $this->view->translate('Remove Operator.');
        $this->view->remove_message = $this->view->translate('The operator will be deleted. After that, you have no way to recover it.');
        $this->renderScript( $this->getRequest()->getControllerName().'/remove.phtml' );

        if ($this->_request->getPost()) {

            Operators_Manager::remove($_POST['id'], $this->connector);
            $this->_redirect($this->getRequest()->getModuleName().'/'.$this->getRequest()->getControllerName());
        }
    }

}
