<?php

/**
 * Q-Manager Operator Statistics Controller
 *
 * @category  Snep
 * @package   Q-Manager
 * @copyright Copyright (c) 2016 OpenS Tecnologia
 * @author    Tiago Zimmermann <tiago.zimmermann@opens.com.br>
 */
class SnepQManager_OperatorReportController extends Zend_Controller_Action {

	/**
     * Initial settings of the class
     */
    public function init() {

        $this->view->url = $this->getFrontController()->getBaseUrl() . '/' . $this->getRequest()->getControllerName();
        $this->view->lineNumber = Zend_Registry::get('config')->ambiente->linelimit;

        $this->connector = "http://127.0.0.1:3000/report/agents?";
        $this->connectorAgents = "http://127.0.0.1:3000/config/agents";

        $this->view->baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl();
        $this->view->key = Snep_Dashboard_Manager::getKey(
            Zend_Controller_Front::getInstance()->getRequest()->getModuleName(),
            Zend_Controller_Front::getInstance()->getRequest()->getControllerName(),
            Zend_Controller_Front::getInstance()->getRequest()->getActionName());
    }

    /**
     * indexAction
     */
    public function indexAction() {

        $this->view->breadcrumb = Snep_Breadcrumb::renderPath(array(
                    $this->view->translate("Q-Manager"),
                    $this->view->translate("Operator Report")));

        $locale = Snep_Locale::getInstance()->getLocale();
        $this->view->datepicker_locale =  Snep_Locale::getDatePickerLocale($locale) ;

        $agents = Operators_Manager::getAll($this->connectorAgents);
        $this->view->agents = $agents;

        $this->view->classRangeDate = "invisible";

        $this->view->translate("answer");
        $this->view->translate("noanswer");

        if ($this->_request->getPost()) {

            $formData = $this->_request->getParams();
            $this->viewAction();
        }

    }

    /**
     * viewAction
     */
    public function viewAction(){

        $formData = $this->_request->getParams();

        $dateFormat = QueueManager_Functions::FormatDateSelect($formData['period'],$formData['from'],$formData['to']);
        $allAgents = "";
        $param = "from=".$dateFormat['start_date']."&to=".$dateFormat['end_date'];

        // request in array, as an API to receive a queue IN time
        foreach($formData['operator'] as $q => $operator){
            $req[$q] = $param."&agent=".$operator;
            $allAgents .= "#".$operator.",";
        }

        // agent Init for tab view
        $agentInit = explode("agent=", $req[0]);
        $this->view->agentInit = $agentInit[1];
        // end agentInit

        foreach($req as $i => $request){

            $service_url = $this->connector.$request;
            $service_url = str_replace(" ", "%20", $service_url);
            $agent = explode("agent=", $service_url);
            $http = curl_init($service_url);
            curl_setopt($http, CURLOPT_SSL_VERIFYPEER, false);
            $status = curl_getinfo($http, CURLINFO_HTTP_CODE);
            curl_setopt($http, CURLOPT_RETURNTRANSFER,1);
            $http_response = curl_exec($http);
            $httpcode = curl_getinfo($http, CURLINFO_HTTP_CODE);
            curl_close($http);

            switch ($httpcode) {
                case 200:
                    $response = json_decode($http_response);
                    if(isset($response->status) == 'Error'){
                        $message = $this->view->translate("Error: ") . $response->message;
                        $this->_helper->redirector('sneperror','error','default',array('error_message'=>$message));
                    }else{
                        $data[$agent[1]] = $response;
                    }
                    break;
                default:
                    $this->view->error = $this->view->translate("Error: Code ") . $httpcode . $this->view->translate(". Please contact the administrator.");
                    break;
            }
        }

        // form save
        $this->view->$formData['period'] = "selected";

        $agents = Operators_Manager::getAll($this->connectorAgents);
        foreach ($formData["operator"] as $key => $operator) {
            foreach($agents as $x => $agent){
                if($operator == $agent->agent){
                    $agents[$x]->selected = true;
                }
            }
        }

        $this->view->agents = $agents;

        if($formData['period'] == "rangedate"){
            $this->view->from = $formData['from'];
            $this->view->to = $formData['to'];
            $this->view->classRangeDate = "";
        }
        // end form save

        $json = QueueManager_Format::formatter('operator','view',$data);
        $data = json_decode($json);

        if(isset($formData['showcsv'])){

            $output = QueueManager_Functions::operatorsCsv($data);
            $fileName = 'Operators_' .substr($dateFormat['start_date'], 0,10)."_".substr($dateFormat['end_date'], 0,10).".csv";

            header("Content-type: text/csv");
            header("Cache-Control: no-store, no-cache");
            header("Content-Disposition: attachment; filename=$fileName");

            echo $output;
            exit;

        }else{

            $this->view->data_init  = date("d/m/Y H:i:s", strtotime($dateFormat['start_date']));
            $this->view->data_end   = date("d/m/Y H:i:s", strtotime($dateFormat['end_date']));
            $this->view->data       = $data;
            $this->view->$allAgents = substr($allAgents,0,-1);
            $this->renderScript('operator-report/index.phtml');
        }

    }

}
